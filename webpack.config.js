// Webpack v4
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
module.exports = {
  entry: { main: './src/index.js' },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'main.js'
	},

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.sass$/,
        loader: ExtractTextPlugin.extract({
					fallback: 'style-loader?sourceMap',
					use: "css-loader?modules&sourceMap=true&importLoaders=1&localIdentName=[name]__[local]__[hash:base64:5]&camelCase=true!postcss-loader!sass-loader?sourceMap"
				})
      }
    ]
	},
	resolve: {
    extensions: ['.js', '.sass']
  },

  plugins: [ 
    new ExtractTextPlugin({filename: 'style.css',  allChunks: true})
  ]
};