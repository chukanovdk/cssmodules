import bluestyle from './style';
import greenstyle from './app.module';

let html = `
<h2 class="${bluestyle.myCssSelector}">I should be displayed in blue.</h2>
 
<h2 class="${greenstyle.myCssSelector}">I should be displayed in green.</h2>
`; 
document.write(html);